SAMPLE GCM SERVER
===========================

digunakan untuk mengirimkan push notification ke device android
berdasarkan gcm registrationId yg sudah teregister (tersimpan di database)

koneksi ke database menggunakan datasource di app server (di contoh ini digunakan jetty)
 
 <New id=gcmTrainingDS class="org.eclipse.jetty.plus.jndi.Resource">
     <Arg></Arg>
     <Arg>jdbc/OrganikaDB</Arg>
     <Arg>
       <New class="com.jolbox.bonecp.BoneCPDataSource">
         <Set name="driverClass">com.mysql.jdbc.Driver</Set>
         <Set name="jdbcUrl">jdbc:mysql://organika-db.nostratech.com:3306/gcm_training</Set>
         <Set name="username">organika</Set>
         <Set name="password">welcome1</Set>
        <Set name="idleConnectionTestPeriodInMinutes">1</Set>
    	<Set name="idleMaxAgeInMinutes">4</Set>
    	<Set name="maxConnectionsPerPartition">30</Set>
    	<Set name="minConnectionsPerPartition">1</Set>
    	<Set name="poolAvailabilityThreshold">5</Set> 
    	<Set name="partitionCount">1</Set>
    	<Set name="acquireIncrement">3</Set>
    	<Set name="statementsCacheSize">50</Set>
    	<Set name="releaseHelperThreads">3</Set>
    	<Set name="connectionTestStatement">select 1</Set>
    	<Set name="lazyInit">true</Set>
      </New>
    </Arg>
  </New>

 
* Buka URL http://host:port/notif/index.jsp untuk test dari browser

