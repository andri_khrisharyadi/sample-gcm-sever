package com.nostratech.gcm.notification.dao;

import com.nostratech.gcm.notification.domain.GcmUser;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
public class GcmDao extends BaseDao<GcmUser> {

    private static final Logger LOGGER = Logger.getLogger(GcmDao.class.getSimpleName());

    public GcmUser findByKey(String gcmKey) {
        Query query = getEntityManager().createQuery("SELECT a FROM GcmUser a WHERE a.gcmKey = :GCM_KEY");
        query.setParameter("GCM_KEY", gcmKey);
        try {
            GcmUser result = (GcmUser) query.getSingleResult();
            return result;
        } catch (NoResultException ex) {
            return null;
        }
    }


    public GcmUser findByName(String name) {
        Query query = getEntityManager().createQuery("SELECT a FROM GcmUser a WHERE a.name = :NAME");
        query.setParameter("NAME", name);
        try {
            GcmUser result = (GcmUser) query.getSingleResult();
            return result;
        } catch (NoResultException ex) {
            return null;
        }
    }

    public String findKeyByName(String name) {
        GcmUser find = findByName(name);
        return (find != null) ? find.getGcmKey() : null;
    }

    public boolean register(String key, String name) {

        GcmUser data = findByKey(key);
        if (data != null) {
            update(data);
            LOGGER.info("update existing data");
            return true;
        } else {
            GcmUser newData = new GcmUser();
            newData.setGcmKey(key);
            newData.setName(name);
            newData = save(newData);

            LOGGER.info("add new data");
            return (newData.getId() != null);
        }

    }


    public boolean unregister(String key) {

        GcmUser data = findByKey(key);
        if (data != null) {
            delete(data.getId());
            LOGGER.info("delete/unregister existing data");
            return true;
        } else {
            return true;
        }

    }

    public List<String> findAllKeys() {
        Query query = getEntityManager().createQuery("SELECT a.gcmKey FROM GcmUser a");
        List<String> result = query.getResultList();
        return result != null ? result : new ArrayList<String>();
    }

}
