package com.nostratech.gcm.notification.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.lang.reflect.ParameterizedType;

/**
 * Created with IntelliJ IDEA.
 * User: andri.khrisharyadi@gmail.com
 * Date: 11/6/13
 * Time: 7:39 PM
 * Base class DAO
 * <T> entity (domain) class
 */
public abstract class BaseDao<T> {

    private Class<T> persistentClass;

    public BaseDao() {
        this.persistentClass =
                (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /*
    * get Entity manager
    */
    public EntityManager getEntityManager() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();

        return em;
    }

    /*
    * save domain class
    */
    public T save(T entity) {
        final EntityManager em = this.getEntityManager();

        EntityTransaction trx = em.getTransaction();

        try {

            trx.begin();

            entity = em.merge(entity);

            trx.commit();

            return entity;

        } catch (RuntimeException e) {
            if (trx != null && trx.isActive()) {
                trx.rollback();
            }
            throw e;
        } finally {
            //Close the manager
            em.close();

        }
    }

    /*
    * update domain class
    */
    public void update(T entity) {
        final EntityManager em = this.getEntityManager();
        EntityTransaction trx = em.getTransaction();

        try {

            trx.begin();

            em.merge(entity);

            trx.commit();

        } catch (RuntimeException e) {
            if (trx != null && trx.isActive()) {
                trx.rollback();
            }
            throw e;
        } finally {
            //Close the manager
            em.close();

        }
    }

    /*
    * delete domain class
    */
    public void delete(Object ID) {

        final EntityManager em = this.getEntityManager();

        EntityTransaction trx = em.getTransaction();

        try {

            T entity = em.find(persistentClass, ID);

            trx.begin();

            em.remove(entity);

            trx.commit();

        } catch (RuntimeException e) {
            if (trx != null && trx.isActive()) {
                trx.rollback();
            }
            throw e;
        } finally {
            //Close the manager
            em.close();

        }
    }

    /*
    * find domain class by id
    */
    public T find(Object ID) {

        final EntityManager em = this.getEntityManager();

        try {

            return em.find(persistentClass, ID);

        } catch (RuntimeException e) {
            throw e;
        } finally {
            //Close the manager
            em.close();

        }
    }

}
