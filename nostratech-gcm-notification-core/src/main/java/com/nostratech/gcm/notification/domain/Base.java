package com.nostratech.gcm.notification.domain;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: aguswinarno
 * Date: 10/11/13
 * Time: 3:06 PM
 */
@MappedSuperclass
public class Base {

    @TableGenerator(name = "SEQ_GENERATOR", table = "SEQUENCE_TABLE",
            pkColumnName = "SEQ_NAME", valueColumnName = "SEQ_COUNT",
            pkColumnValue = "BASE_SEQ")
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE,
            generator = "SEQ_GENERATOR")
    protected Integer id;

    @Version
    @Column(name = "version")
    protected Integer version;

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return version;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
