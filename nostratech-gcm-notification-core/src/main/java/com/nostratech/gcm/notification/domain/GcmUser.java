package com.nostratech.gcm.notification.domain;

import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
@Entity
@Table(name = "GCM_USERS")
@Indexes( value = {
    @Index( name = "GCM_USER_GCM_KEYS_IDX", columnNames = {"gcm_key"}, unique = true),
    @Index( name = "GCM_USER_NAME_IDX", columnNames = {"name"}, unique = false)     }
)
public class GcmUser extends Base {

    @Column(name = "name")
    private String name;

    @Column(name = "gcm_key", nullable = false, unique = true)
    private String gcmKey;

    public void setGcmKey(String gcmKey) {
        this.gcmKey = gcmKey;
    }

    public String getGcmKey() {
        return gcmKey;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
