package com.nostratech.gcm.notification.gcm;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

public class GcmClient {

    private final static Logger LOGGER = Logger.getLogger(GcmClient.class.getSimpleName());

    private  final String GCM_SERVER_KEY = "AIzaSyC8__7htqhNNW59O3zGQktgvbEX25Pvhxg";

    private final String REQUEST_URL = "https://android.googleapis.com/gcm/send";

    private Random random = new Random();

    private Gson gson = new Gson();

    public GcmClient() {
    }

    /**
     * Sends a http GCM message.
     */
    public void send(List<String> recipients, String message) throws IOException {


        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(REQUEST_URL);
        HttpResponse response;

        LOGGER.info("Start send message to GCM server to : ("+recipients.toString()+") with data : "+message);

        httpPost.setHeader("Authorization", "key=".concat(GCM_SERVER_KEY));
        httpPost.setHeader("Content-Type", "application/json");

        String jsonRequest = createJsonMessage(recipients, message);

        httpPost.setEntity(new StringEntity(jsonRequest, ContentType.APPLICATION_JSON));
        httpclient.execute(httpPost);

        //Get the response
        response = httpclient.execute(httpPost);

            /*Checking response */
        if (response != null) {

            // Print result
            LOGGER.info("Response : " + response.toString());

            int responseCode = response.getStatusLine().getStatusCode();
            String responseText = Integer.toString(responseCode);
            LOGGER.info("Response code from GCM server : "+ responseText);

            InputStream in = response.getEntity().getContent(); //Get the data in the entity
            String responseBody = getStringFromInputStream(in);
            LOGGER.info("Response body from GCM server : "+ responseBody);


        }

    }

    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }


    public String createJsonMessage(List<String> recipients, String sendMessage) throws IOException {
        Map<String, Object> message = new HashMap<String, Object>();
        message.put("registration_ids", recipients);

        message.put("collapse_key", String.valueOf(random.nextLong()) );

        // default 4 weeks
        //message.put("time_to_live", 2419200l);

        message.put("delay_while_idle", true);

        Map data = new HashMap();
        data.put("message", sendMessage);

        message.put("data", data);

        String gcmPayload = gson.toJson(message);
        LOGGER.info("GCM message payload : "+gcmPayload);
        return gcmPayload;
    }

}
