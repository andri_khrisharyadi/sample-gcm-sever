package com.nostratech.gcm.notification.dao;

import com.nostratech.gcm.notification.domain.GcmUser;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
public class GcmDaoTest {

    private GcmDao gcmDao = new GcmDao();

    @Test
    public void testAddGcm(){

        boolean resultAdd = gcmDao.register("testGcm123", "andri");
        boolean resultUpdate = gcmDao.register("testGcm123", "andri");
        Assert.assertTrue(resultAdd && resultUpdate);
    }

}
