package com.nostratech.gcm.notification.rest;

import com.nostratech.gcm.notification.domain.Employee;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/18/15.
 */
@Path("/person")
public class EmployeeService {

    private static final Logger LOGGER = Logger.getLogger(EmployeeService.class.getSimpleName());

    private List<Employee> listEmployee = new ArrayList<Employee>();

    public EmployeeService(){
        listEmployee.add(new Employee(1, "budi", "budi@gmail.com", "081901444", "jakarta"));
    }

    @Path("/")
    @Consumes("application/json")
    @Produces("application/json")
    @PUT
    public Response addEmployee(Employee employee){
        listEmployee.add(employee);
        return Response.status(200).entity("new employee id "+employee.getId()).build();
    }

    @Path("/")
    @Produces("application/json")
    @GET
    public Response findAllEmployee(){
        return Response.status(200).entity(listEmployee).build();
    }

    @Path("/{id}")
    @Produces("application/json")
    @GET
    public Response findById(@PathParam("id") Integer id){
        Employee employee = listEmployee.get(id);
        if(employee != null) return Response.status(200).entity(employee).build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Path("/{id}")
    @Produces("application/json")
    @DELETE
    public Response DeleteById(@PathParam("id") int id){
        Employee result = listEmployee.remove(id);
        if(result != null) return Response.status(200).build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}
