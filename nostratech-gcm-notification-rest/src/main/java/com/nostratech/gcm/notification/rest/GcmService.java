package com.nostratech.gcm.notification.rest;

import com.nostratech.gcm.notification.dao.GcmDao;
import com.nostratech.gcm.notification.gcm.GcmClient;
import com.nostratech.gcm.notification.model.GcmModel;
import com.nostratech.gcm.notification.model.GcmResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
@Path("/gcm")
public class GcmService {

    private GcmDao gcmDao;
    private GcmClient gcmClient;

    public GcmService(){
        gcmDao = new GcmDao();
        gcmClient = new GcmClient();
    }

    @POST
    @Path("/register")
    @Consumes("application/json")
    @Produces("application/json")
    public Response registerUser(GcmModel model) {
        GcmResponse response;
        boolean result = gcmDao.register(model.getGcmKey(), model.getName());
        if(result) response = new GcmResponse(200, "OK", "OK");
        else response = new GcmResponse(400, "NOT OK", "Register GCM FAILED");
        return Response.status(200).entity(response).build();
    }

    @POST
    @Path("/unregister")
    @Consumes("application/json")
    @Produces("application/json")
    public Response unregisterUser(GcmModel model) {

        boolean result = gcmDao.unregister(model.getGcmKey());
        GcmResponse response = new GcmResponse(200, "OK", "OK");
        return Response.status(200).entity(response).build();
    }

    @POST
    @Path("/sendByName")
    @Consumes("application/json")
    @Produces("application/json")
    public Response sendByName(GcmModel model){

        String gcmKey = gcmDao.findKeyByName(model.getName());
        if(gcmKey != null) {
            try {
                List<String> recipients = new ArrayList<String>();
                recipients.add(gcmKey);
                gcmClient.send(recipients, model.getMessage());
            } catch (IOException ex) {
                return Response.status(200).entity(new GcmResponse(400, "NOT OK", "Failed send message")).build();
            }

        }
        return Response.status(200).entity(new GcmResponse(200, "OK", "OK")).build();
    }

    @POST
    @Path("/sendAll")
    @Consumes("application/json")
    @Produces("application/json")
    public Response sendAll(GcmModel model){
        List<String> regIds = gcmDao.findAllKeys();
        if(!regIds.isEmpty()) {
            try {
                gcmClient.send(regIds, model.getMessage());
            } catch (IOException ex) {
                return Response.status(200).entity(new GcmResponse(400, "NOT OK", "Failed send message")).build();
            }

        }
        return Response.status(200).entity(new GcmResponse(200, "OK", "OK")).build();
    }


    @GET
    @Path("/allKeys")
    @Produces("application/json")
    public Response findAllKey(){
        List<String> regIds = gcmDao.findAllKeys();
        return Response.status(200).entity(regIds).build();
    }

}
