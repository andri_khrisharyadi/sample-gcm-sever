package com.nostratech.gcm.notification.rest;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
public class RestApplication extends Application {

    private Set<Object> singletons = new HashSet<Object>();

    public RestApplication() {
        singletons.add(new GcmService());
        singletons.add(new EmployeeService());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
