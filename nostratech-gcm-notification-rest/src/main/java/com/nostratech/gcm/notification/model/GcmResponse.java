package com.nostratech.gcm.notification.model;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
public class GcmResponse {

    private int code;

    private String status;

    private String message;

    public GcmResponse() {}

    public GcmResponse(int code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
