package com.nostratech.gcm.notification.model;

/**
 * Created by andri.khrisharyadi@gmail.com on 9/8/15.
 */
public class GcmModel {

    private String name;

    private String gcmKey;

    private String message;

    public void setGcmKey(String gcmKey) {
        this.gcmKey = gcmKey;
    }

    public String getGcmKey() {
        return gcmKey;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
