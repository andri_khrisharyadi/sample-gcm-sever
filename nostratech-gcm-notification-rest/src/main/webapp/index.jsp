<%--
    Document   : index
    Created on : Oct 7, 2013, 3:12:09 PM
    Author     : andri sasuke
--%>
<%--
    Document   : index
    Created on : Oct 7, 2013, 3:12:09 PM
    Author     : andri sasuke
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>GCM Server message</title>
    <script language="JavaScript" src="js/jquery-1.10.2.min.js"></script>

    <script language="javascript">

        $(document).ready(function(){

            $("#bt_send_by_name").click(function(){

                var tName = $("#t_name").val();
                var tMessage = $("#t_message").val();

                var jsonObjects = { name: tName, message: tMessage };
                jQuery.ajax({
                    url: "rest/gcm/sendByName",
                    type: "POST",
                    data: JSON.stringify(jsonObjects),
                    contentType:"application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result) {
                        alert( "message sent to "+tName);
                    },
                    error: function(result) {
                        alert( "message is failed to sent");
                    }
                });

            });


            $("#bt_send_all").click(function(){

                var tName = $("#t_name").val();
                var tMessage = $("#t_message").val();

                var jsonObjects = {  message: tMessage };
                jQuery.ajax({
                    url: "rest/gcm/sendAll",
                    type: "POST",
                    data: JSON.stringify(jsonObjects),
                    contentType:"application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result) {
                        alert( "message sent to all" );
                    },
                    error: function(result) {
                        alert( "message is failed to sent");
                    }
                });
            });

        });

    </script>

</head>

<body>
<div>
    <div class="header">
        <H3>Send a message</H3>
    </div>
</div>
<div id="container">
    <div id="content">
        <p>Name</p>
        <p><input type="text" id="t_name" name="t_name" /></p>

        <p>Message</p>
        <p><textarea id="t_message" name="t_message" rows="4" cols="30" > </textarea> </p>

        <p>
            <input type="button" id="bt_send_by_name" name="bt_send_by_name" value="Send By Name" />
            <input type="button" id="bt_send_all" name="bt_send_all" value="Send All" />
        </p>

    </div>
</div>
</body>
</html>
